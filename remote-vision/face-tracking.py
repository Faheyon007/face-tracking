import dlib
import cv2
import face_recognition as fr


face_filepath_db = [
    "01.jpg"
]

face_labels = [
    "Harry"
]

# known_face_encodings = []


images_basepath = "E:\\Development\\Hitachi\\remote-vision\\remote-vision\\images"
videos_basepath = "E:\\Development\\Hitachi\\remote-vision\\remote-vision\\videos"


def show_image(filepath):
    img = fr.load_image_file(filepath)
    bgr_img = img[:, :, ::-1]
    cv2.imshow("", bgr_img)
    cv2.waitKey(0)


def test():
        # show_image("images\\01.jpg")

    vc = cv2.VideoCapture("videos\\01.mp4")

    if not vc.isOpened():
        print("IOError : Could not open video file!")
        exit(-1)

    while vc.isOpened():
        status, frame = vc.read()

        if not status:
            break

        rgb_frame = frame[:, :, ::-1]

        cv2.imshow("", frame)
        cv2.waitKey(1)

    vc.release()
    cv2.destroyAllWindows()


def test_crop_face():
    known_face_encodings = []
    global face_labels

    face_image = fr.load_image_file(
        f"{images_basepath}\\01.jpg")
    known_face_locations = fr.face_locations(face_image)

    for fl in known_face_locations:
        top, right, bottom, left = fl

        face = face_image[top:bottom, left:right, :]
        face_bgr = face[:, :, ::-1]
        # cv2.imwrite(
        #     "01-face.jpg", face_bgr)
        face_bgr = cv2.putText(face_bgr, face_labels[0], (0, len(face_bgr)),
                               cv2.FONT_HERSHEY_PLAIN, 2, (0, 255, 255), 1)
        cv2.imshow("face", face_bgr)
        cv2.waitKey(1)

        fe = fr.face_encodings(face)
        known_face_encodings.append(fe[0])

        # face_rec = cv2.rectangle(face_image, (left, top),
        #                          (right, bottom), (0, 255, 0), 2)
        # cv2.imshow("face_rec", face_rec[:, :, ::-1])

    test_img = fr.load_image_file(f"{images_basepath}\\08.jpg")
    test_img_bgr = test_img[:, :, ::-1]

    face_locations = fr.face_locations(test_img)
    face_encodings = fr.face_encodings(test_img, face_locations)

    for t, r, b, l in face_locations:
        test_img_bgr = cv2.rectangle(
            test_img_bgr, (l, t), (r, b), (255, 0, 0), 2)

    cv2.imshow("test_img", test_img_bgr)
    cv2.waitKey(1)

    print(f"Faces found : {len(face_locations)}")

    face_names = []
    for face_encoding in face_encodings:
        match = fr.compare_faces(known_face_encodings, face_encoding)

        for i in range(len(match)):
            if match[i]:
                print(f"Match found : {face_labels[i]}")
                face_names.append(face_labels[i])
            else:
                face_names.append("Unknown")

    for name, (t, r, b, l) in zip(face_names, face_locations):
        test_img_bgr = cv2.putText(
            test_img_bgr, name, (l, b), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 255), 1)

    cv2.imshow("Labeled faces", test_img_bgr)
    cv2.waitKey(0)


def test_face_tracking():

    known_face_encodings = []
    known_face_labels = []

    person_counter = 1

    img = fr.load_image_file(f"{images_basepath}\\08.jpg")
    cv2.namedWindow("img", cv2.WINDOW_NORMAL)
    cv2.imshow("img", img[:, :, ::-1])
    cv2.waitKey(100)

    face_locations = fr.face_locations(img)

    img_face_rect = img
    for t, r, b, l in face_locations:
        img_face_rect = cv2.rectangle(
            img_face_rect, (l, t), (r, b), (0, 255, 0), 2)
        name = f"Person {person_counter}"
        person_counter += 1
        known_face_labels.append(name)
        cv2.putText(img_face_rect, name, (l, b),
                    cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 255), 1)
        cv2.imshow("img", img_face_rect[:, :, ::-1])
        cv2.waitKey(100)

    known_face_encodings = fr.face_encodings(img, face_locations)

    while True:
        img = fr.load_image_file(f"{images_basepath}\\09.jpg")

        face_locations = fr.face_locations(img)

        img_face_rect = img
        for t, r, b, l in face_locations:
            img_face_rect = cv2.rectangle(
                img_face_rect, (l, t), (r, b), (0, 255, 0), 2)
            cv2.namedWindow("img_test", cv2.WINDOW_NORMAL)
            cv2.imshow("img_test", img_face_rect[:, :, ::-1])
            cv2.waitKey(100)

        face_encodings = fr.face_encodings(img, face_locations)

        for face_encoding, (t, r, b, l) in zip(face_encodings, face_locations):
            match = fr.compare_faces(known_face_encodings, face_encoding)

            match_found = False
            for i in range(len(match)):
                if match[i]:
                    match_found = True
                    cv2.putText(img_face_rect, known_face_labels[i], (
                        l, b), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 255), 1)
                    break

            if not match_found:
                name = f"Person {person_counter}"
                person_counter += 1
                known_face_labels.append(name)
                cv2.putText(img_face_rect, name, (l, b),
                            cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2)
                known_face_encodings.append(
                    fr.face_encodings(img[t:b, l:r], [(t, r, b, l)])[0])

            cv2.imshow("img_test", img_face_rect[:, :, ::-1])
            cv2.waitKey(100)


def test_face_tracking_video():
    known_face_encodings = []
    known_face_labels = []

    person_counter = 1

    img = fr.load_image_file(f"{images_basepath}\\08.jpg")
    cv2.namedWindow("img", cv2.WINDOW_NORMAL)
    cv2.imshow("img", img[:, :, ::-1])
    cv2.waitKey(100)

    face_locations = fr.face_locations(img)

    img_face_rect = img
    for t, r, b, l in face_locations:
        img_face_rect = cv2.rectangle(
            img_face_rect, (l, t), (r, b), (0, 255, 0), 2)
        name = f"Person {person_counter}"
        person_counter += 1
        known_face_labels.append(name)
        cv2.putText(img_face_rect, name, (l, b),
                    cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 255), 1)
        cv2.imshow("img", img_face_rect[:, :, ::-1])
        cv2.waitKey(100)

    known_face_encodings = fr.face_encodings(img, face_locations)

###########################################################################
#
###########################################################################

    vc = cv2.VideoCapture(f"{videos_basepath}\\01.mp4")

    if not vc.isOpened():
        print("IOError : Could not open Camera!")
        exit(-1)

    cv2.namedWindow("img_test", cv2.WINDOW_NORMAL)

    while vc.isOpened():
        status, bgr_img = vc.read()

        if not status:
            break

        cv2.imshow("img_test", bgr_img)
        cv2.waitKey(1)

        img = bgr_img[:, :, ::-1]

        face_locations = fr.face_locations(img)

        img_face_rect = img
        for t, r, b, l in face_locations:
            img_face_rect = cv2.rectangle(
                img_face_rect, (l, t), (r, b), (0, 255, 0), 2)
            # cv2.imshow("img_test", cv2.cvtColor(
            #     img_face_rect, cv2.COLOR_RGB2BGR))
            # cv2.waitKey(1)

        face_encodings = fr.face_encodings(img, face_locations)

        for face_encoding, (t, r, b, l) in zip(face_encodings, face_locations):
            match = fr.compare_faces(known_face_encodings, face_encoding)

            match_found = False
            for i in range(len(match)):
                if match[i]:
                    match_found = True
                    cv2.putText(img_face_rect, known_face_labels[i], (
                        l, b), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 255), 1)
                    break

            if not match_found:
                name = f"Person {person_counter}"
                person_counter += 1
                known_face_labels.append(name)
                cv2.putText(img_face_rect, name, (l, b),
                            cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2)
                known_face_encodings.append(
                    fr.face_encodings(img[t:b, l:r], [(t, r, b, l)])[0])

            cv2.imshow("img_test", cv2.cvtColor(
                img_face_rect, cv2.COLOR_RGB2BGR))
            cv2.waitKey(1)


if __name__ == "__main__":
    # test()
    # test_crop_face()
    # test_face_tracking()
    test_face_tracking_video()

    cv2.destroyAllWindows()
