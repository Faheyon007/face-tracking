import numpy as np
import matplotlib.pyplot as plt
import cv2
import dlib


audio_samples = [{'azimuth': 10.75, 'vertical': 55.21},
    {'azimuth': 11.15, 'vertical': 52.67},
    {'azimuth': 13.71, 'vertical': 56.16},
    {'azimuth': 9.33, 'vertical': 54.45},
    {'azimuth': 12.46, 'vertical': 56.19},
    {'azimuth': 10.01, 'vertical': 53.98},]


audio_sample_avg = {'azimuth': 10.56, 'vertical': 55.12}


mic_coords = [{'x': -10.0, 'y': -5.0, 'z': 0.0},
    {'x': -5.0, 'y': -5.0, 'z': 0.0},
    {'x': 0.0, 'y': -5.0, 'z': 0.0},
    {'x': 5.0, 'y': -5.0, 'z': 0.0},
    {'x': 10.0, 'y': -5.0, 'z': 0.0}]

camera_coord = {'x': 0.0, 'y':0.0, 'z': 0.0 }