import cv2
import dlib
import face_recognition as fr


tracker_types = [
    "csrt",
    "kcf",
    "boosting",
    "mil",
    "tld",
    "medialflow",
    "mosse"
]


def getTracker(tracker_type):

    tracker = None

    if tracker_type == "csrt":
        tracker = cv2.TrackerCSRT_create()
    elif tracker_type == "kcf":
        tracker = cv2.TrackerKCF_create()
    elif tracker_type == "boosting":
        tracker = cv2.TrackerBoosting_create()
    elif tracker_type == "mil":
        tracker = cv2.TrackerMIL_create()
    elif tracker_type == "tld":
        tracker = cv2.TrackerTLD_create()
    elif tracker_type == "medianflow":
        tracker = cv2.TrackerMedianFlow_create()
    elif tracker_type == "mosse":
        tracker = cv2.TrackerMOSSE_create()
    else:
        print('ValueError: Please specify a valid tracker type!')

    return tracker


def track(tracker_type):
    vc = cv2.VideoCapture("remote-vision\\videos\\01.mp4")

    if not vc.isOpened():
        print('IOError: Can not open video stream!')
        exit(-1)

    tracker = None
    tracking_BB = None

    while vc.isOpened():
        status, frame = vc.read()
        frame_processing = cv2.resize(frame, (0, 0), fx=0.5, fy=0.5)

        if tracking_BB is not None:
            status, bbox = tracker.update(frame)
            if status:
                x, y, w, h = bbox
                cv2.rectangle(frame, (int(x), int(y)),
                              (int(x+w), int(y+h)), (0, 255, 255))

        cv2.imshow('', frame)
        key = cv2.waitKey(15) & 0xFF

        if key == ord('s'):
            tracker = cv2.TrackerCSRT_create()
            tracking_BB = cv2.selectROI('', frame, True, False)
            w, h = tracking_BB[2:]
            if not (w < 3 or h < 3):
                tracker.init(frame, tracking_BB)
        elif key == ord('q'):
            break

    vc.release()
    cv2.destroyAllWindows()


def track_face(tracker_type):
    vc = cv2.VideoCapture("remote-vision\\videos\\01.mp4")

    if not vc.isOpened():
        print('IOError: Can not open video stream!')
        exit(-1)

    tracker = None
    tracking_BB = None

    while vc.isOpened():
        status, frame = vc.read()
        frame_processing = cv2.resize(frame, (0, 0), fx=0.5, fy=0.5)

        if tracking_BB is not None:
            # check if the BB contains face
            x, y, w, h = tracking_BB
            frame_BB = frame[y:y+h, x:x+w]

            cv2.imshow("frame_BB", frame_BB)
            cv2.waitKey(1)

            # face = fr.face_locations(frame_BB)

            # if len(face) <= 0:
            #     tracking_BB = None
            # else:
            status, bbox = tracker.update(frame)
            if status:
                print("Tracking Successful")
                x, y, w, h = bbox
                cv2.rectangle(frame, (int(x), int(y)),
                              (int(x+w), int(y+h)), (0, 255, 255), 3)
            else:
                print("Tracking Failed")
                tracking_BB = None

        if tracking_BB is None:
            face_locations = fr.face_locations(frame[:, :, ::-1])
            if len(face_locations) > 0:
                tracker = getTracker(tracker_type)
                top, right, bottom, left = face_locations[0]
                x, y, w, h = left, top, right - left, bottom - top
                tracking_BB = x, y, w, h
                tracker.init(frame, tracking_BB)

        cv2.imshow('', frame)
        key = cv2.waitKey(1) & 0xFF

        # if key == ord('s'):
        #     tracker = cv2.TrackerCSRT_create()
        #     tracking_BB = cv2.selectROI('', frame, True, False)
        #     w, h = tracking_BB[2:]
        #     if not (w < 3 or h < 3):
        #         tracker.init(frame, tracking_BB)
        if key == ord('q'):
            break

    vc.release()
    cv2.destroyAllWindows()


if __name__ == "__main__":
    track_face("mosse")
