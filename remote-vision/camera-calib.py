import cv2
import numpy as np
import glob

def save_images():

    max_frame_count = 20

    cap = cv2.VideoCapture(0)

    if not cap.isOpened():
        print("OSError: Could not open capture device!")
        exit(-1)

    print("\npress 'space' to start capturing.\npress 'q' to quit".upper())
    capture = False
    frame_counter = 0
    while cap.isOpened():

        status, frame = cap.read()

        if not status:
            print("ReadError: No frames to read!")
            break

        cv2.imshow("", frame)

        if capture == True:
            frame_counter += 1
            print(f"[ frame {frame_counter}/{max_frame_count}] read - ok".upper())
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            cv2.imwrite(f"remote-vision/calib/images/{frame_counter}.jpg", frame)
            print(f"[ frame {frame_counter}/{max_frame_count}] write - ok".upper())

        key = cv2.waitKey(500)
        # print(f"key pressed : {key}")

        if key == ord("q"):
            break
        elif key == 32:
            if capture:
                print("[ capture paused ]".upper())
            elif capture == False and frame_counter > 1:
                print("[ capture resumed ]".upper())
            else:
                print("[ capture begin ]".upper())
            capture = not capture

        if frame_counter == max_frame_count:
            frame_counter = 0
            capture = False
            print("[ capture finished ]".upper())

    cap.release()
    cv2.destroyAllWindows()


def save_images_after_success():

    object_points = []
    image_points = []

    objp = np.zeros((7 * 6, 3), np.float32)
    objp[:, :2] = np.mgrid[0:7, 0:6].T.reshape(-1, 2)

    max_frame_count = 20

    capture = False

    cap = cv2.VideoCapture(0)

    if not cap.isOpened():
        print("OSError: Could not open capture device!")
        exit(-1)

    # print("\npress 'space' to start capturing.\npress 'q' to quit".upper())
    # capture = False
    frame_counter = 0
    while cap.isOpened():

        status, frame = cap.read()

        if not status:
            print("ReadError: No frames to read!")
            break

        # print(f"[ frame {frame_counter}/{max_frame_count}] read - ok".upper())
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        ret = False
        if capture:
            print("finding chessboard corners : ".title(), end="")
            ret, corners = cv2.findChessboardCorners(frame, (7, 6))

            if ret:
                print("success".upper())
                frame_counter += 1
                cv2.imwrite(f"remote-vision/calib/images/{frame_counter}.jpg", frame)
                print(f"[ frame {frame_counter}/{max_frame_count}] write - ok".upper())

                object_points.append(objp)
                image_points.append(corners)
                corners2 = cv2.cornerSubPix(
                    frame,
                    corners,
                    (11, 11),
                    (-1, -1),
                    (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001),
                )

                # with open(f"remote-vision/calib/image-points/{frame_counter}.txt", "wt") as f:
                #     f.write(f"{corners}")
                # print(f"[ image point {frame_counter}/{max_frame_count} ] write - ok".upper())

                # with open(f"remote-vision/calib/object-points/{frame_counter}.txt", "wt") as f:
                #     f.write(f"{objp}")
                # print(f"[ object point {frame_counter}/{max_frame_count} ] write - ok".upper())

                cv2.drawChessboardCorners(frame, (7, 6), corners2, ret)
            else:
                print("failed".upper())
            
        cv2.imshow("", frame)
        if ret:
            key = cv2.waitKey(1000)
        else:
            key = cv2.waitKey(1)

        if key == ord("q"):
            break
        elif key == 32:
            if capture:
                print("[ capture paused ]".upper())
            elif capture == False and frame_counter > 1:
                print("[ capture resumed ]".upper())
            else:
                print("[ capture begin ]".upper())
            capture = not capture

        if frame_counter == max_frame_count:
            frame_counter = 0
            capture = False
            print("[ capture finished ]".upper())

            with open("remote-vision/calib/image-points.txt", "wt") as f:
                f.write(f"{image_points}")
            with open("remote-vision/calib/object-points.txt", "wt") as f:
                f.write(f"{object_points}")

    cap.release()
    cv2.destroyAllWindows()


def find_chessboard_corners():

    object_points = []
    image_points = []

    objp = np.zeros((7 * 6, 3), np.float32)
    objp[:, :2] = np.mgrid[0:7, 0:6].T.reshape(-1, 2)

    max_frame_count = 20

    filenames = glob.glob("./remote-vision/calib/images/*.jpg")

    counter = 1
    file_count = len(filenames)

    frame = None
    for filename in filenames:
        frame = cv2.imread(filename, cv2.CV_8UC1)

        print(f"[{counter}/{file_count}] finding chessboard corners : ".title(), end="")
        ret, corners = cv2.findChessboardCorners(frame, (7, 6))

        if ret:
            print("success".upper())

            object_points.append(objp)
            image_points.append(corners)
            corners2 = cv2.cornerSubPix(
                frame,
                corners,
                (11, 11),
                (-1, -1),
                (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001),
            )

            cv2.drawChessboardCorners(frame, (7, 6), corners2, ret)
        else:
            print("failed".upper())
            
        cv2.imshow("", frame)
        if ret:
            key = cv2.waitKey(1000)
        else:
            key = cv2.waitKey(1)
        counter += 1

    np.save("./remote-vision/calib/image-points.npy", np.array(image_points))
    np.save("./remote-vision/calib/object-points.npy", np.array(object_points))

    ip_arr = np.load("./remote-vision/calib/image-points.npy")
    op_arr = np.load("./remote-vision/calib/object-points.npy")
    # print(f"Array : {arr}")
    print(f"Shape : {ip_arr.shape}")
    print(f"Shape : {op_arr.shape}")

    # with open("remote-vision/calib/image-points.txt", "wt") as f:
    #     f.write(f"{image_points}")
    # with open("remote-vision/calib/object-points.txt", "wt") as f:
    #     f.write(f"{object_points}")

    ret, cam_mat, dis_coeff, rot_vec, tran_vec = cv2.calibrateCamera(op_arr, ip_arr, frame.shape[::-1], None, None)
    
    cv2.destroyAllWindows()


def calibrate():
    pass


if __name__ == "__main__":
    # save_images()
    # save_images_after_success()
    find_chessboard_corners()