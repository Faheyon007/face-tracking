# Real-Time Face Recognition & Tracking

## Objective

- Face detection
- Real-time face recognition
- Real-time face learning
- Track faces both known and unknown faces